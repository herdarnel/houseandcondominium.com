@extends('layouts.master')
@section('content')
  @foreach ($home as $content)
    @section('title', $content->title)
    @section('image', Voyager::image( setting('site.site_image') ))
  <div class="home-banner">

    @component('components.banner', ['banners' => $banners])
    @endcomponent

    @component('components.search', ['cities' => $cities])
      @slot('title')
         Find a Home
      @endslot
      @slot('class')
         home
      @endslot
    @endcomponent
  </div>

  
  @component('components.location', ['cities' => $cities])
    @slot('title')
        Check Properties in Cebu
    @endslot
  @endcomponent

  @component('components.featured_listing', ['properties' => $houses])
    @slot('title')
        Featured House & Lot
    @endslot
     @slot('description')
        What’s your future home like? <br> Aside from being accessible and convenient, make sure that it has put in place preventive measures in a time of crisis. Remember, your home should be your first line of defense against risks, from earthquakes to break-ins down to viral infections.  
    @endslot
    @slot('url')
        {{ route('houselot') }}
    @endslot
  @endcomponent

  @component('components.featured_listing', ['properties' => $condominiums])
    @slot('title')
        Featured Condominium
    @endslot
    @slot('description')
        Whether you're single or married, having your own  space can mean much to our sense of direction, responsibility and our confidence. Homeownership provides you full autonomy on how to start and end your day or how you would like to run your household. And ultimately live in peace. 
    @endslot
    @slot('url')
        {{ route('condominium') }}
    @endslot
  @endcomponent

  @component('components.featured_blog', ['posts' => $posts])
    @slot('title')
        Featured Condominium
    @endslot
    @slot('url')
        {{ route('condominium') }}
    @endslot
  @endcomponent
  


  @endforeach
@endsection