<section class="section section-testimonials">
	<div class="container">
		<h3 class="section-title text-center">{{ $title }}</h3>
		<!-- Set up your HTML -->
		<div class="owl-carousel owl-carousel-testimonial">
			{{-- @for ($i = 0; $i < 4 ; $i++) --}}
			@foreach ($testimonials as $testimonial)
			
			  <div class="testimonial-item">
			  	<div class="comment">
			  		{{$testimonial->body}}
			  	</div>
			  	<div class="client-img">
			  		<img src="{{ Voyager::image( $testimonial->image ) }}" alt="" class="img-circle">
			  	</div>
			  	<div class="client-name">
			  		<p>{{$testimonial->name}}</p>
			  	</div>
			  </div>
			@endforeach
			{{-- @endfor --}}
		</div>
	</div>
</section>