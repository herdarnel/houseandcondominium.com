<div class="row">
	@foreach ($properties as $property)
		<div class="col-12 col-md-6 col-lg-4">
			<div class="card mb-4 box-shadow property-item">
			    <div class="parent" onclick="">
			        <div class="child"  style="background-image: url({{ Voyager::image( $property->image ) }});">
			            <a href="{{ route('property',['category'=> $property->property_type, 'slug'=>$property->slug]) }}"><span class="fa fa-eye"></span></a>
			        </div>
			    </div>
                <div class="card-body">
				  <div class="features">
				  	<ul>
				  		<li><p><i class="fa fa-bed"></i> {{$property->bedrooms ?? ""}}</p></li>
				  		<li><p><i class="fa fa-bath"></i> {{$property->toilet_bath ?? ""}}</p></li>
				  		<li><p><i class="fa fa-car"></i> {{$property->parking ?? ""}}</p></li>
				  	</ul>
				  </div>
				  <div class="details">
				  	<p class="name"><i class="fa fa-home"></i> {{ $property->title }}</p>
				  	<p class="address"><i class="fa fa-map-marker"></i>{{ $property->address }}</p>
			        <p class="price"><span>₱</span> {{ $property->price }}</p>
				  </div>
                </div>
            </div>
		</div>
	@endforeach
	<div class="col-12">
	    <!-- Bootstrap Pagination-->
	    {{$properties->links('partials.paging')}}
	</div>
</div>