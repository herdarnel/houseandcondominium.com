@php
  if(\Route::current()->getName() == 'home') {
    $sold_properties = $sold_properties;
  } else {
    $sold_properties = '';
  }
@endphp

@component('components.agent_info', ['properties' => $properties, 'sold_properties' => $sold_properties])
  @slot('title')
      Contact Us
  @endslot
@endcomponent


@component('components.testimonials', ['testimonials' => $testimonials])
  @slot('title')
      What our Client Say
  @endslot
@endcomponent


@component('components.awards', ['developers' => $developers])
  @slot('title')
      What our Client Say
  @endslot
@endcomponent


<footer>
  <div class="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <p>{!! setting('site.site_description') !!}</p>
        </div>
        <div class="col-md-4">
          <div class="social-media text-right">
            <ul class="social-media">
              <li><a href="{{ setting('contact.facebook') }}"><i class="fa fa-facebook"></i></a></li>
              <li><a href="{{ setting('contact.instagram') }}"><i class="fa fa-instagram"></i></a></li>
              <li><a href="{{ setting('contact.youtube') }}"><i class="fa fa-youtube"></i></a></li>
            </ul>

          </div>
        </div>
      </div>
    </div>
  </div>
</footer>