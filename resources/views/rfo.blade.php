@extends('layouts.master')
@section('content')
	@foreach ($page as $content)
    @section('title', $content->title)
    @section('image', Voyager::image( setting('site.site_image') ))
    <section class="section category-page">
      <div class="container">
        <div class="text-center">
          <h2 class="section-title">All</h2>
        </div>

        <div class="houseandlot-body">
          <div class="row">
              <div class="col-md-2 max-width">
                


                <div class="d-none d-sm-block">
                  <h5 class="provinces-text">Property Type</h5>
                  <ul class="provinces">
                      <li @if ('house-and-lot' == request()->segment(count(request()->segments()))) class="active" @endif>
                        <a class="nav-link" href="house-and-lot">House and Lot</a>
                      </li>
                      <li @if ('condominium' == request()->segment(count(request()->segments()))) class="active" @endif>
                        <a class="nav-link" href="condominium">Condominium</a>
                      </li>
                  </ul>
                </div>


                <div class="dropdown d-sm-none mb-3 location-mobile">
                  <button class="btn btn-red btn-block dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Property Tye
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="house-and-lot">House and Lot</a>
                    <a class="dropdown-item" href="condominium">Condominium</a>
                  </div>
                </div>


              </div>
             
              <div class="col-md-10">
                <section class="project-pages">
                  <div class="container">

                    @if (count($properties) > 0)
                      @component('components.property_listing', ['properties' =>$properties])
                      @endcomponent
                    @else
                      <h4>Properties coming soon..</h4>
                    @endif
                </section>
              </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>
	@endforeach
@endsection