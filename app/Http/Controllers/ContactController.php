<?php

namespace App\Http\Controllers;
use App\Page;
use App\Contact;
use App\Property;
use App\Developer;
use App\Testimonial;
use App\Mail\ContactMail;
use Mail;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index(){

        $page = Page::where('id', 7)->get();
        $properties =  Property::select('title')->get();
        $testimonials =  Testimonial::all();
        $developers = Developer::orderBy('id', 'asc')->get();

        return view('contact', compact('page', 'properties', 'testimonials', 'developers'));
    }
    
    public function contactStore(Request $data){

        $this->validate($data, [
            'name' => 'required',
            'email' => 'required|email',
            'phone'=>'required',
            'location'=>'required',
            'message' => 'required'
        ]);

	    Contact::create($data->all());

	    $contact = array(
	           'name' => $data->post('name'),
	           'email' => $data->post('email'),
	           'phone' => $data->post('phone'),
	           'location' => $data->post('location'),
	           'property_type' => $data->post('property_type'),
	           'message' => $data->post('message'),

	       );

	    $site_settings = array(
	        'mail_name' => setting('contact.mail_name'),
	        'mail_address' => setting('contact.email'),
	        'cc' => 'christine.gonx@gmail.com',
	    );

	    Mail::send('email', ['contact' => $contact, 'site_settings' => $site_settings], function($message) use ($contact, $site_settings) {
	       $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
	       $message->to($site_settings['mail_address'], $site_settings['mail_name']);
	       $message->cc($site_settings['cc'])->subject('Inquiries in House and Condominium');
	    });

	    // dd(url()->previous());
	    $prev = url()->previous();
	    // $url = $prev->path();

	    // dd($prev);
	    if ($prev != "http://127.0.0.1:8000/contact") {
	    	return redirect($prev)->with('success_message', 'Thank you for your interest. We will contact you soon regarding your inquiry!');
	    } else {
	    	return redirect()->route('contact.index')->with('success_message', 'Thanks for contacting us!');
	    }
        
    }
}
