<div class="search-wrapper">
      {{-- <h2>{{ $title }}</h2> --}}
  <form action="{{ route('search') }}" action="get">
    <div class="input-group">
      <input type="text" class="form-control" name="property" placeholder="Preferred Location" aria-label="Find a Home" aria-describedby="basic-addon2">
      <div class="input-group-append">
        <button class="btn btn-red btn-block">Search</button>
      </div>
    </div>

{{--     <div class="row">
      <div class="col-5 col-sm-6 col-md-7">
        <input type="text" class="form-control" name="property" placeholder="Property Name">
      </div>
      <div class="col-4 col-sm-3 col-md-3 pl-0">
        <select name="location" id="" class="form-control">
          <option value="">Location</option>
          @foreach ($cities as $city)
            <option value="{{$city->slug}}">{{$city->city}}</option>
          @endforeach
        </select>
      </div>
      <div class="col-3 col-sm-3 col-md-2 pl-0">
        <button class="btn btn-red btn-block">Search</button>
      </div>
    </div> --}}
  </form>
</div>