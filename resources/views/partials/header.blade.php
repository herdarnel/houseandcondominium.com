<div class="header-top">
  <div class="container-fluid">
    <div class="row">
      <div class="col-6 col-sm-8 col-md-9">
        <ul class="contact-info">
          <li><a  href="tel:{{ setting('agent-info.contact_number') }}"><i class="fa fa-phone"></i>{{ setting('agent-info.contact_number') }}</a></li>
          <li class="d-none d-md-inline"><a href="{{ route('contact.index') }}"><i class="fa fa-map-marker"></i>{{setting('contact.office')}}</a></li>
        </ul>
      </div>
      <div class="col-6 col-sm-4 col-md-3 text-right">
        <ul class="social-media">
            <li><a href="{{ setting('contact.facebook') }}"><i class="fa fa-facebook"></i></a></li>
            <li><a href="{{ setting('contact.instagram') }}"><i class="fa fa-instagram"></i></a></li>
            <li><a href="{{ setting('contact.youtube') }}"><i class="fa fa-youtube"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<nav class="main-navbar navbar navbar-expand-md navbar-light">
  <div class="container">
    <a class="navbar-brand" href="{{ route('home') }}">
      <img src="{{ Voyager::image( setting('site.logo') ) }}" alt="">
    </a>
    <button id="nav-icon3" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </button>

    <div class=" navbar-collapse collapse" id="navbarSupportedContent">
      {{menu('header','menu.header')}}
      {{-- <div class="send-message-navbar">
        <a href="https://www.messenger.com/t/HousesAndCondominiumInCebu/" target="_blank" class="btn btn-red messenger-btn">Send Message</a>
      </div> --}}
    </div>
  </div>
</nav>