@extends('layouts.master')
@section('content')
  <div class="category-page">
    @section('image', Voyager::image( setting('site.site_image') ))
    @foreach ($page as $content)
  
    @section('title', $content->title)

      @php
        $route =  Route::current();
        $name = $route->getName();

        if(($name == "houselot") || ($name == "housecity")) {
          $passroute = "housecity";
        } elseif(($name == "condominium") || ($name == "condominiumcity")) {
          $passroute = "condominiumcity";
        }

        if(isset($currentCity)) {
          $title = $content->title." | ".$currentCity;
        } else {
          $title = $content->title;
        }
        
      @endphp

      @section('title', $title)
      <section class="section singlepage-height">
        <div class="container">
          <div class="text-center">
            <h2 class="section-title">{{$currentCity ?? "ALL"}}</h2>
          </div>

          <div class="houseandlot-body">
            <div class="row">
              <div class="col-12 col-sm-3 col-md-2 max-width">
                
                <div class="d-none d-sm-block">
                  <h5 class="provinces-text">Locations</h5>
                  <ul class="provinces">
                    @foreach ($cities as $city)
                      
                      <li @if ($city->slug == request()->segment(count(request()->segments()))) class="active" @endif>
                        <a class="nav-link"  href="{{route($passroute ,['city'=> $city->slug ])}}">{{ $city->city }}</a>
                      </li>
                    @endforeach
                  </ul>
                </div>


                <div class="dropdown d-sm-none mb-3 location-mobile">
                  <button class="btn btn-red btn-block dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Locations
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach ($cities as $city)
                        <a  class="dropdown-item"  href="{{route($passroute ,['city'=> $city->slug ])}}">{{ $city->city }}</a>
                    @endforeach
                  </div>
                </div>


              </div>
             
              <div class="col-12 col-sm-9 col-md-10">
                <section class="project-pages">
                  <div class="container">
                    @if (count($properties) > 0)
                      @component('components.property_listing', ['properties' =>$properties])
                      @endcomponent
                    @else
                      <h4>Properties coming soon..</h4>
                    @endif

                </section>
              </div>
            </div>
          </div>
        </div>
      </section>
    @endforeach  </div>
@endsection