@extends('layouts.master')
@section('content')
  <div class="category-page">
    @section('title', "Search Results")
    @section('image', Voyager::image( setting('site.site_image') ))
    @foreach ($page as $content)
       <div class="house-lot-banner single-banner" style="background-image: url('{{ Voyager::image( $content->image ) }}');">
        <div class="overlay"></div>
         <div class="container">
            @component('components.search', ['cities' => $cities])
              @slot('title')
                 Find a Home
              @endslot
              @slot('class')
                 search-page
              @endslot
            @endcomponent
        </div>
       </div>

      <section class="section singlepage-height">
        <div class="container">
          <div class="text-center">
            <h2 class="section-title">Search Result(s)</h2>
          </div>

          <div class="houseandlot-body">
            <div class="row">
              <div class="col-md-12">
                <section class="project-pages">
                  <div class="container">
                    @if (count($properties) > 0)
                      @component('components.property_listing', ['properties' =>$properties])
                      @endcomponent
                    @else
                      <h3>No property found. Forgot the keyword? Check on our <a href="{{ route('houselot') }}">House & Lot </a> and <a href="{{ route('condominium') }}">Condominium</a> property listing..</h3>
                    @endif

                </section>
              </div>
            </div>
          </div>
        </div>
      </section>
    @endforeach  </div>
@endsection