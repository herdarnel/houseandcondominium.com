<section class="section blue-section section-location">
	<div class="container">
		<h3 class="section-title text-center">{{ $title }}</h3>
		<p class="text-center mb-3">Should you be buying a house now? <br> All that matters now, aside from amount cash you have on hand, is the house and the community you and your family live in. </p>
		<div class="row">
			@foreach ($cities->slice(0, 4) as $city)
				<div class="col-6 col-md-3 mt-4">
					<a class="city-wrapper" href="{{ route('housecity', ['city'=> $city->slug]) }}">
					    <div class="city-img" style="background-image: url({{ Voyager::image( $city->image ) }});">
					    </div>
					    <div class="city">{{$city->city}}</div>
					</a>
				</div>
			@endforeach
		</div>
	</div>
</section>