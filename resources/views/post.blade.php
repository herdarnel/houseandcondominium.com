@extends('layouts.master')
@section('content')
  @foreach ($posts as $post)
  @section('title', $post->title)
    @section('image', Voyager::image( setting('site.site_image') ))
    <section class="post-page">
     <div class="container">
        <h1 class="mt-5">{{ $post->title }}</h1>
        <div class="row mb-5">
          <div class="col-6">
            <img src="{{ Voyager::image( $post->image ) }}" alt="{{ $post->title }}">
          </div>
          <div class="col-6">
            <div class="description mb-5">
              {!! $post->body !!}
            </div>
          </div>
        </div>
        
        
      </div>  
    </section>
  @endforeach
@endsection