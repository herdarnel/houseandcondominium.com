<section class="owl-slider-banner owl-carousel owl-theme">
	@foreach ($banners as $banner)
    	<div class="item" style="background-image: url({{ Voyager::image( $banner->image ) }});">
    		<div class="banner-text">
    			<h2>Find your Home</h2>
    			<p> {{ $banner->subtitle }}</p>
    		</div>
    	</div>
	@endforeach
</section>