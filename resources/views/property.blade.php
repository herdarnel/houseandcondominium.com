@extends('layouts.master')
@section('content')
@php
  $image = Voyager::image($property->image);
@endphp
@section('title', $property->title)
@section('image', $image)
  <section class="property-page">
    <div class="container">
      <div class="panel mb-5">
        <div class="row">
          <div class="col-md-6 col-lg-9">
            <h4 class="project-name">{{$property->title}}</h4>
            <p class="address"><i class="fa fa-map-marker"></i>{{$property->address}}</p>
              @if ($property->property_type == "House & Lot")
                <ul class="area">
                  <li><p>Lot Area: {{$property->lot_area}}</p></li>
                  <li><p>Floor Area: {{$property->floor_area}}</p></li>
                </ul>
              @endif
          </div>
          <div class="col-md-6 col-lg-3">
            <p class="price">₱ {{$property->price}}</p>
            @if ($property->property_type == "House & Lot")
              <ul class="features">
                <li>
                  <p>
                  @if ($property->bedrooms)
                   <i class="fa fa-bed"></i> {{$property->bedrooms}}
                  @else
                  Studio
                  @endif
                  </p>
                </li>
                <li><p><i class="fa fa-bath"></i> {{$property->toilet_bath }}</p></li>
                <li><p><i class="fa fa-car"></i> {{ $property->parking ?? "0" }}</p></li>
              </ul>
            @endif
          </div>
          <div class="col-md-12">
            <div class="photogallery">
              <div id="sync1" class="owl-carousel owl-theme owl-propery-gallery">
                {{-- @for ($i = 0; $i < 8; $i++) --}}
                  @if ($property->image)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image) }}" class="item" style="background-image: url({{ Voyager::image($property->image) }})"></a>
                  @endif
                  @if ($property->image_1)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_1) }}" class="item" style="background-image: url({{ Voyager::image($property->image_1) }})"></a>
                  @endif
                  @if ($property->image_2)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_2) }}" class="item" style="background-image: url({{ Voyager::image($property->image_2) }})"></a>
                  @endif
                  @if ($property->image_3)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_3) }}" class="item" style="background-image: url({{ Voyager::image($property->image_3) }})"></a>
                  @endif
                  @if ($property->image_4)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_4) }}" class="item" style="background-image: url({{ Voyager::image($property->image_4) }})"></a>
                  @endif
                  @if ($property->image_5)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_5) }}" class="item" style="background-image: url({{ Voyager::image($property->image_5) }})"></a>
                  @endif
                  @if ($property->image_6)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_6) }}" class="item" style="background-image: url({{ Voyager::image($property->image_6) }})"></a>
                  @endif
                  @if ($property->image_7)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_7) }}" class="item" style="background-image: url({{ Voyager::image($property->image_7) }})"></a>
                  @endif
                  @if ($property->image_8)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_8) }}" class="item" style="background-image: url({{ Voyager::image($property->image_8) }})"></a>
                  @endif
                  @if ($property->image_9)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_9) }}" class="item" style="background-image: url({{ Voyager::image($property->image_9) }})"></a>
                  @endif
                  @if ($property->image_10)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_10) }}" class="item" style="background-image: url({{ Voyager::image($property->image_10) }})"></a>
                  @endif
                  @if ($property->image_11)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_11) }}" class="item" style="background-image: url({{ Voyager::image($property->image_11) }})"></a>
                  @endif
                  @if ($property->image_12)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_12) }}" class="item" style="background-image: url({{ Voyager::image($property->image_12) }})"></a>
                  @endif
                {{-- @endfor --}}
              </div>

              <div id="sync2" class="owl-carousel owl-theme owl-propery-gallery">
                  @if ($property->image)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image) }}" class="item" style="background-image: url({{ Voyager::image($property->image) }})"></a>
                  @endif
                  @if ($property->image_1)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_1) }}" class="item" style="background-image: url({{ Voyager::image($property->image_1) }})"></a>
                  @endif
                  @if ($property->image_2)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_2) }}" class="item" style="background-image: url({{ Voyager::image($property->image_2) }})"></a>
                  @endif
                  @if ($property->image_3)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_3) }}" class="item" style="background-image: url({{ Voyager::image($property->image_3) }})"></a>
                  @endif
                  @if ($property->image_4)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_4) }}" class="item" style="background-image: url({{ Voyager::image($property->image_4) }})"></a>
                  @endif
                  @if ($property->image_5)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_5) }}" class="item" style="background-image: url({{ Voyager::image($property->image_5) }})"></a>
                  @endif
                  @if ($property->image_6)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_6) }}" class="item" style="background-image: url({{ Voyager::image($property->image_6) }})"></a>
                  @endif
                  @if ($property->image_7)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_7) }}" class="item" style="background-image: url({{ Voyager::image($property->image_7) }})"></a>
                  @endif
                  @if ($property->image_8)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_8) }}" class="item" style="background-image: url({{ Voyager::image($property->image_8) }})"></a>
                  @endif
                  @if ($property->image_9)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_9) }}" class="item" style="background-image: url({{ Voyager::image($property->image_9) }})"></a>
                  @endif
                  @if ($property->image_10)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_10) }}" class="item" style="background-image: url({{ Voyager::image($property->image_10) }})"></a>
                  @endif
                  @if ($property->image_11)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_11) }}" class="item" style="background-image: url({{ Voyager::image($property->image_11) }})"></a>
                  @endif
                  @if ($property->image_12)
                    <a data-fancybox="gallery" href="{{ Voyager::image($property->image_12) }}" class="item" style="background-image: url({{ Voyager::image($property->image_12) }})"></a>
                  @endif
              </div>

            </div>
          </div>
          <div class="col-md-4 col-lg-3 d-none d-sm-none d-md-block">
            <div class="sidebar mt-3">
              <div class="agent-info">
                <div class="agent-img">
                  <img src="{{ Voyager::image( setting('agent-info.image') ) }}" alt="">
                </div>
                <div class="agent-details">
                  <h2 class="name">{{setting('agent-info.name')}}</h2>
                  <p class="position text-center">{{setting('agent-info.position')}}</p>
                  <p><i class="fa fa-facebook"></i> <a href="//facebook.com/{{ setting('agent-info.facebook_page') }}" target="_blank" class="fb-link">{{ setting('agent-info.facebook_page') }}</a></p>
                  <p><i class="fa fa-mobile"></i> <a href="tel:{{ setting('agent-info.contact_number') }}">{{ setting('agent-info.contact_number') }}</a></p>
                  <p><i class="fa fa-envelope"></i> <a href="mailto:{{ setting('contact.email') }}">{{ setting('contact.email') }}</a></p>
                </div>
                <div class="brokerage mt-4">
                  <h2>Brokerage/Company</h2>
                  <img src="{{ Voyager::image( setting('agent-info.brokerage') ) }}" style="width: 30%;display: block;margin: 0 auto;" alt="">
                </div>
                <hr>
                <div class="sharethis-inline-share-buttons"></div>
                <hr>
                <div class="contact-form-wrapper mt-4">
                  <h2>Inquire this property</h2>
                  @if (session()->has('success_message'))
                      <div class="spacer"></div>
                      <div class="alert alert-success">
                          {{ session()->get('success_message') }}
                      </div>
                  @endif

                  @if(count($errors) > 0)
                      <div class="spacer"></div>
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{!! $error !!}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif
                  <form action="{{ route('contact.store') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="location" value="{{ $property->title }}">
                    <input type="hidden" name="property_type" value="{{ $property->property_type }}">
                    <div class="form-group">
                      <label for="exampleInputName">Name *</label>
                      <input type="name" class="form-control" name="name" id="exampleInputName" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email address *</label>
                      <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPhone">Phone *</label>
                      <input type="phone" class="form-control" name="phone" id="exampleInputPhone">
                    </div>
                    <div class="form-group">
                      <label for="exampleFormControlTextarea1">Message *</label>
                      <textarea class="form-control" name="message" id="exampleFormControlTextarea1" rows="6"></textarea>
                    </div>
                    <button type="submit" class="btn btn-block btn-red">Submit</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-8 col-lg-9 pl-0 mt-3">
            <div class="property-section">
              <div class="property-nav">
                <ul>
                  <li><a href="#property-details">Property Details</a></li>
                  <li><a href="#location">Location</a></li>
                  @if ($property->vtour)
                  <li><a href="#vtour" data-toggle="modal" data-target=".vTour">Virtual Tour</a></li>
                  @endif
                  {{-- <li><a href="#sas" data-toggle="modal" data-target=".sas">Schedule a Showing</a></li> --}}
                  <li><a href="//facebook.com/{{ setting('agent-info.facebook_page') }}" target="_blank">Computation</a></li>
                </ul>
              </div>
              <div class="property-detail-parts" id="property-details">
                <h2 class="">Property Details</h2>
                <div class="details">
                  {!! $property->body !!}
                </div>
              </div>
              <div class="property-detail-parts" id="location">
                <h2 class="">Map Location</h2>

                    @php
                      $mapAddress = $property->title.",".$property->address;
                    @endphp

                <iframe height="400" id="gmap_canvas" src="https://maps.google.com/maps?q={{$mapAddress}}&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
              </div>
              @if ($property->vtour)
              <div class="property-detail-parts" id="vtour">
                <h2 class="">Virtual Tour</h2>
                <a href="#"  data-toggle="modal" data-target=".vTour">
                  <img src="{{ Voyager::image( $property->image ) }}" alt="" class="img-responsive">
                </a>
              </div>
              @endif
              <div class="property-detail-parts d-sm-block d-md-none">
                <h2>Share this property</h2>
                <div class="sharethis-inline-share-buttons"></div>
              </div>
              <div class="property-detail-parts d-sm-block d-md-none">
                
                <div class="contact-form-wrapper mt-4">
                  <h2>Inquire this property</h2>
                  @if (session()->has('success_message'))
                      <div class="spacer"></div>
                      <div class="alert alert-success">
                          {{ session()->get('success_message') }}
                      </div>
                  @endif

                  @if(count($errors) > 0)
                      <div class="spacer"></div>
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{!! $error !!}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif
                  <form action="{{ route('contact.store') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="location" value="{{ $property->title }}">
                    <input type="hidden" name="property_type" value="{{ $property->property_type }}">
                    <div class="form-group">
                      <label for="exampleInputName">Name *</label>
                      <input type="name" class="form-control" name="name" id="exampleInputName" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email address *</label>
                      <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPhone">Phone *</label>
                      <input type="phone" class="form-control" name="phone" id="exampleInputPhone">
                    </div>
                    <div class="form-group">
                      <label for="exampleFormControlTextarea1">Message *</label>
                      <textarea class="form-control" name="message" id="exampleFormControlTextarea1" rows="6"></textarea>
                    </div>
                    <button type="submit" class="btn btn-block btn-red">Submit</button>
                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </section>

@if ($property->vtour)
<!-- Modal -->
<div class="modal fade vTour" tabindex="-1" role="dialog" aria-labelledby="vTourLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <iframe src="https://www.youtube.com/embed/{{$property->vtour}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>
</div>
@endif

@if (count($relatedProperty) > 0)
  @component('components.featured_listing', ['properties' => $relatedProperty])
    @slot('title')
        Other properties in {{$property->city}}
    @endslot
    @slot('description')
       Dont wait to buy real estate, buy real estate and wait
    @endslot
    @slot('url')
        {{ route('housecity', ['city' => $property->city]) }}
    @endslot
  @endcomponent
@endif

@endsection

@section('js')
    <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5eb3b1b6f4076c0012cc07f9&product=inline-share-buttons" async="async"></script>
@endsection