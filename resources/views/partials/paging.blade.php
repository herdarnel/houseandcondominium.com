@if ($paginator->lastPage() > 1)
    @php
        $link_limit = 5;
    @endphp
    <nav aria-label="Page navigation">
        <ul class="pagination pagination-custom float-right">
            <li class="page-item page-item-control {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
                <a class="page-link" href="{{($paginator->currentPage() == 1) ? '#' : $paginator->previousPageUrl() }}" aria-label="Previous"><span class="fa fa-chevron-left" aria-hidden="true"></span>
                </a>
            </li>
            @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                @php
                $half_total_links = floor($link_limit / 2);
                $from = $paginator->currentPage() - $half_total_links;
                $to = $paginator->currentPage() + $half_total_links;
                if ($paginator->currentPage() < $half_total_links) {
                    $to += $half_total_links - $paginator->currentPage();
                }
                if ($paginator->lastPage() - $paginator->currentPage() < $half_total_links) {
                    $from -= $half_total_links - ($paginator->lastPage() - $paginator->currentPage()) - 1;
                }
                @endphp
                @if ($from < $i && $i < $to)
                    <li class="page-item {{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                    <span class="page-link">
                        @if($paginator->currentPage() == $i)
                            {{$i}}
                        @else
                            <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
                        @endif
                    </span>
                    </li>
                @endif
            @endfor
            <li class="page-item page-item-control {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
                <a class="page-link" href="{{($paginator->currentPage() == $paginator->lastPage()) ? '#' : $paginator->nextPageUrl() }}" aria-label="Next">
                    <span class="fa fa-chevron-right" aria-hidden="true"></span>
                </a>
            </li>
        </ul>
    </nav>
@endif
