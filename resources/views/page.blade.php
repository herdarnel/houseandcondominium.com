@extends('layouts.master')
@section('content')
  @foreach ($page as $content)
    @section('title', $content->title)
    @section('image', Voyager::image( setting('site.site_image') ))
     <div class="contactus-banner single-banner" style="background-image: url('{{ Voyager::image( $content->image ) }}');">
       <div class="overlay"></div>
       <div class="container">
        <div class="single-banner-title">
           <h1>{{ $content->title }}</h1>
         </div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $content->title }}</li>
          </ol>
        </nav>
      </div>
     </div>

  <section class="about-description mt-5">
    <div class="container">
      <div class="text-center">
        <h2 class="section-title">{{ $content->title }}</h2>
      </div>

      <div class="body mb-5">
        {!! $content->body !!}
      </div>

    </div>


  </section>
  @endforeach
@endsection