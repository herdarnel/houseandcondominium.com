<section class="section section-featured {{ $class ?? "" }}">
	<div class="container">
		<h3 class="section-title text-center">{{ $title }}</h3>
		<p class="text-center mb-4">{{ $description }}</p>
		<div class="row">
			{{-- @for ($i = 0; $i < 3; $i++) --}}
			@foreach ($properties as $property)
				<div class="col-md-4">
					<div class="card mb-4 box-shadow property-item">
					    <div class="parent" onclick="">
					        <div class="child"  style="background-image: url({{ Voyager::image( $property->image ) }});">
					            <a href="{{ route('property',['category'=> $property->property_type, 'slug'=>$property->slug]) }}"><span class="fa fa-eye"></span></a>
					        </div>
					    </div>
		                <div class="card-body">
						  <div class="features">
						  	<ul>
						  		<li><p><i class="fa fa-bed"></i> {{$property->bedrooms ?? ""}}</p></li>
						  		<li><p><i class="fa fa-bath"></i> {{$property->toilet_bath ?? ""}}</p></li>
						  		<li><p><i class="fa fa-car"></i> {{$property->parking ?? ""}}</p></li>
						  	</ul>
						  </div>
						  <div class="details">
						  	<p class="name"><i class="fa fa-home"></i>{{ $property->title }}</p>
						  	<p class="address"><i class="fa fa-map-marker"></i>{{ $property->address }}</p>
					        <p class="price"><span>₱</span> {{ $property->price }}</p>
						  </div>
		                </div>
		            </div>
				</div>
			@endforeach
			{{-- @endfor --}}
			<div class="col-md-12 text-center">
				<a href="{{ $url }}" class="btn btn-red">See More</a>
			</div>
		</div>
	</div>
</section>