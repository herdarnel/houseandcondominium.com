@extends('layouts.master')
@section('content')
  @foreach ($page as $content)
  @section('title', $content->title)
    @section('image', Voyager::image( setting('site.site_image') ))
     <div class="contactus-banner single-banner" style="background-image: url('{{ Voyager::image( $content->image ) }}');">
       <div class="overlay"></div>
       <div class="container">
        <div class="single-banner-title">
           <h1>{{ $content->title }}</h1>
         </div>
      </div>
     </div>
   
     <section>

        <div class="contactus-body" style="height: 400px;">
          <iframe height="100%" width="100%" id="gmap_canvas" src="https://maps.google.com/maps?q=ASR Realty and Brokerage Company&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
        </div>
      {{-- </div> --}}
    </section>
  @endforeach
@endsection