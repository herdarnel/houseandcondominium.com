<?php

namespace App\Http\Controllers;
use App\Page;
use App\Property;
use App\Developer;
use App\City;
use App\SoldProperty;
use App\Video;
use App\Testimonial;
use App\Post;
use App\Banner;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index($slug){

        $page = Page::where('slug','=',$slug)->first();
        $properties = Property::all();

        if(!$page){
            return redirect('/');
        }

        return view('page', compact('page', 'properties'));
    }

    public function home(){

        $home = Page::where('id', 1)->get();
        $banners = Banner::orderBy('order', 'asc')->get();
        $cities = City::orderBy('id', 'asc')->get();
        $houses =  Property::where('featured', '=', 1)
                            ->where('property_type', '=', 'House & Lot')
                            ->where('status', '=', 'ACTIVE')
                            ->orderBy('order', 'asc')
                            ->get();
        $condominiums =  Property::where('featured', '=', 1)
                            ->where('property_type', '=', 'Condominium')
                            ->where('status', '=', 'ACTIVE')
                            ->orderBy('order', 'asc')
                            ->get();

        $posts =  Post::where('featured', '=', 1)->get();
        $properties =  Property::select('title')->get();
        $sold_properties =  SoldProperty::orderBy('id', 'desc')->take(4)->get();
        $testimonials =  Testimonial::all();
        $developers = Developer::orderBy('id', 'asc')->get();

        return view('home', compact('home', 'banners', 'cities', 'houses', 'condominiums','posts', 'developers', 'properties', 'sold_properties', 'testimonials'));
    }
    
    public function about(){

        $page = Page::where('id', 2)->get();
        $properties =  Property::select('title')->get();
        $testimonials =  Testimonial::all();
        $developers = Developer::orderBy('id', 'asc')->get();

        return view('about', compact('page', 'properties', 'testimonials', 'developers'));
    }
    
    public function videos(){

        $page = Page::where('id', 8)->get();
        $videos =  Video::all();
        $properties =  Property::select('title')->get();
        $testimonials =  Testimonial::all();
        $developers = Developer::orderBy('id', 'asc')->get();

        return view('videos', compact('page', 'videos', 'properties', 'testimonials', 'developers'));
    }
    
    public function post($slug){

        $posts = Post::where('slug',"=", $slug)->get();
        $properties =  Property::select('title')->get();
        $testimonials =  Testimonial::all();
        $developers = Developer::orderBy('id', 'asc')->get();

        return view('post', compact('posts', 'properties', 'testimonials', 'developers'));
    }

    public function page($slug){

        $page = Page::where('slug', $slug)->get();
        $properties =  Property::select('title')->get();
        $testimonials =  Testimonial::all();
        $developers = Developer::orderBy('id', 'asc')->get();

        return view('page', compact('page', 'properties', 'testimonials', 'developers'));
    }
    
}
